FactoryBot.define do
sequence :username do |i|
  Faker::Internet.username + i.to_s
end

  factory :user do
    username
    bio {Faker::Lorem.sentence}
  end
end
