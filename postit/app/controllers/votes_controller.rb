class VotesController < ApplicationController
  def create
    @vote = Vote.new(vote_params)

    if @vote.save
      render json: {vote: @vote}
    else
      render json: {errors: @vote.errors.full_messages}, status: :bad_request
    end
  end

  def vote_params
    params.require(:vote).permit(:kind, :post_id).merge(user_id: User.first.id)
  end
  skip_before_action :verify_authenticity_token

end
