class PagesController < ApplicationController
  def home
    @posts = Post.paginate(page: params[:page], per_page: 10).order(vote_balance: :desc)
  end
end
