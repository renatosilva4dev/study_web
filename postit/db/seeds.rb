# User.destroy_all
# 50.times do
#   word_count = rand(1..100)
#   user = User.new(
#     username: Faker::Internet.username,
#     bio: Faker::Lorem.sentence(word_count: word_count)
#   )
#   if user.save
#     p "User - #{user.username} - criado"
#   end
# end

# Post.destroy_all
# 100.times do
#   user = User.all.sample
#   post = Post.new(
#     user: user,
#     title: Faker::Lorem.sentence(word_count: rand(1..5)),
#     body: Faker::Lorem.paragraph(sentence_count: rand(1..5))
#   )
#
#   if post.save
#     p "Post - #{post.title} - criado com sucesso"
#   end
# end

500.times do
  user = User.all.sample
  vote = Vote.new(
    user: user,
    post: Post.all.sample,
    kind: ["up", "down"].sample
  )
  p "Vote: #{vote.id} - created" if vote.save
end
