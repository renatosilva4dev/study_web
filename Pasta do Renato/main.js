var app = new Vue({
  el: '#produto',
  data: {
    produto: 'Blusa',
    imagem: 'imagem/verde.jpg',
    inStock: true,
    detalhes: ["100% poliester","importado","Dri-fit"],
    variantes: [
      {
        varianteId:1,
        variantColor: "green",
        variantImagem: 'imagem/verde.jpg'
      },
      {
        varianteId:2,
        variantColor: "white",
        variantImagem: 'imagem/branca.jpg'
      }
    ],
    carrinho: 0,
    },
    methods:{
      updateProduct: function (variantImagem){
        this.imagem = variantImagem
      }
    }
})
