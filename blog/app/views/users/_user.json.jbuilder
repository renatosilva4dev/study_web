json.extract! user, :id, :name, :bio, :username, :created_at, :updated_at
json.url user_url(user, format: :json)
