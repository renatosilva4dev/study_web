require "bundler"
Bundler.require

APP_ROOT = File.expand_path(__dir__)

Dir.glob(File.join(APP_ROOT, "models", "*.rb")).each { |file| require file}
# Dir.glob(File.join(APP_ROOT, "helpers", "*.rb")).each { |file| require file}

set :database, {adapter: "sqlite3", database: "database.sqlite3"}


get '/' do
   @tasks = Task.all
   erb :index
end

get '/tasks/:id' do
  @task = Task.find(params[:id])

  erb :form
end

post '/tasks' do
  @task = Task.new
  @task.description = params[:desc]

  if @task.save
    redirect "/"
  else
    erb :form
  end
end

delete "/tasks/:id" do
  @task = Task.find(params[:id])

  @task.destroy
  redirect "/"
end

put '/tasks/:id' do
  @task = Task.find(params[:id])

  if @task.update_attributes({description: params[:desc]})
    redirect '/'
  else
    erb :form
  end
end
