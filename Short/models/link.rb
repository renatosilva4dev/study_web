class Link < ActiveRecord::Base
  after_create :short_link

  def short_link
    token = ShortToken.encode(self.id)
    self.short = "#{ENV['SHORT_BASE_URL']}/#{token}"

    self.save!
  end

  validates :original, presence: true
end
