require "bundler"
Bundler.require

APP_ROOT = File.expand_path(__dir__)

Dir.glob(File.join(APP_ROOT, "models", "*.rb")).each { |file| require file}
Dir.glob(File.join(APP_ROOT, "helpers", "*.rb")).each { |file| require file}

set :database, {adapter: "sqlite3", database: "database.sqlite3"}

Dotenv.load

get '/' do
   @links = Link.all
   erb :index, layout: :main
end

get "/:token" do
  id = ShortToken.decode(params[:token])
  @link = Link.find(id)

  redirect @link.original
end


post "/" do
  @link = Link.where(original: params[:link]).first_or_create
  # if Link.find_by(original: params[:link]).nil?
  #   @link = Link.create(original: params[:link])
  # else
  #   @link = Link.find_by(original: params[:link])
  # end
  content_type :json

  {original: @link.original, short: @link.short}.to_json
end
