module ShortToken
    def self.encode(number)
      hashids = Hashids.new(ENV['SHORT_SALT'])
      hashids.encode(number)
    end

    def self.decode(encode_number)
      hashids = Hashids.new(ENV['SHORT_SALT'])
      hashids.decode(encode_number).first
    end
  end
