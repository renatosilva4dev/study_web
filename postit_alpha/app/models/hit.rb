class Hit < ApplicationRecord
  belongs_to :user
  belongs_to :post

  validates :kind, presence: true, inclusion: {in: ['up', 'down']}

  #TODO

end
