class CreateHits < ActiveRecord::Migration[6.0]
  def change
    create_table :hits do |t|
      t.string :kind
      t.belongs_to :user, null: false, foreign_key: true
      t.belongs_to :post, null: false, foreign_key: true

      t.timestamps
    end
  end
end
